#include <iostream>
#include <vector>
using namespace std;

typedef struct TrieNode {
    int path;//经过数
    int end; //结束数
    std::vector<TrieNode*> nexts;

    TrieNode() {
        path = 0;
        end = 0;
        nexts.reserve(26);
    }
}

class Trie {
private:
    TrieNode* root;

    Trie() {
        root = new TrieNode();
    }
    ~Trie() {
        //遍历析构
    }

    void insertTrieNode(std::string word) {
        if(word.size() == 0) {
            return;
        }

        TrieNode* node = root;

        for(auto n : word) {
            int index = n - 'a';
            if(node->nexts[index]== nullptr) {//没有就新建
                node->nexts[index] = new TrieNode();
                // node = node->nexts[index];
                // node->path++;
                // continue;
            }
            node = node->nexts[index];
            node->path
        }

        node->end++;
    }

    void deleteTrieNode(std::string word) {
        if(searchTrieNode(word) == 0) {   //xxj
            return;
        }

        TrieNode* node = root;

        for(auto n: word) {
            int index = n - 'a';
            if(node->nexts[index] != nullptr) {
                node = node->nexts[index];
                if(--node->path == 0) {
                    delete node;
                    node = nullptr;
                }
            }
        }
        if(node != nullptr)
            --node->end;
    }

    int searchTrieNode(std::string word) {
        if(word.size() == 0) {
            return;
        }
        
        TrieNode* node = root;

        for(auto n : word) {
            int index = n - 'a';
            if(node->nexts[index] != nullptr) {
                node = node->nexts[index];
            } else {
                return 0;
            }
        }

        return node->end; //可能存在end 为0
    }
}
