#include <iostream>
#include <cmath>
#include <vector>
using namespace std;

int getMaxBits(vector<int> nums) {
    int max = 0;
    for(int i = 0; i < nums.size(); i++) {
        if(max < nums[i]) {
            max = nums[i];
        }
    }

    int bit = 0;
    while(max /10 ) {
        bit++;
    }
    return bit;
}

int getNumber(int num , int d) {
    return (num / (int)pow(10,d-1)) % 10; // % 10
}

void Sort(vector<int> nums) {
    if(nums.size() <= 1) {
        return;
    }

    int digit = getMaxBits(nums);
    RadixSort(nums,digit);
}

void RadixSort(vector<int> nums,int digit)
{
    int redix = 10;
    vector<int> bucket(nums.size());
    for(int i = 1; i <= digit; i++) {
        int count[redix] = {0};
        for(int j = 0 ; j< nums.size(); j++ ) {
            int number = getNumber(nums[j],i);
            count[number]++;
        }
        for(int j = 1 ; j < redix; j++) {
            count[j] += count[j-1];
        }

        for(int j = nums.size() -1; j >= 0; j--) { //end -> begin
            int number = getNumber(nums[j],i);
            bucket[count[number] - 1 ] = nums[j]; //-1
            count[number]--;
        }

        nums.swap(bucket);
    }
}