#include <iostream>
#include <vector>
using namespace std;

int GetMax(vector<int> nums) {
    return process(nums,0,nums[nums.size() - 1]);
}

int process (vector<int> nums , int L , int R) {
    if (L == R)
        return nums[L];
    int mid = L + (R - L) >> 1;
    int leftMax = process(nums,L,mid);  //mid + 1
    int rightMax = process(nums,mid+1,R);
    return leftMax>rightMax?leftMax:rightMax;
}