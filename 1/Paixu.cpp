#include <iostream>
#include <vector>
using namespace std;

int SelectSort(vector<int> nums) {
    if (nums.size() == 0 || nums.size() == 1)
        return 0;
    for(auto iter = nums.begin(); iter != nums.end(); iter++) {
        auto minIter = iter;
        for(auto iter1 = iter+1; iter1 != nums.end(); iter1++) {
            minIter = *iter1 < *minIter ? iter : minIter;
        }
        swap(*iter,*minIter);
    }

    return 0;
}

int BubbleSort(vector<int> nums) {
    if (nums.size() == 0 || nums.size() == 1)
        return 0;
    
    for(int i = nums.size() - 1; i >= 0 ; i--) {
        for(int j = 0; j < i; j++) {
            if(nums[j] > nums[j+1]) {
                swap(nums[j],nums[j+1]);
            }
        }
    }
    return 0;
}

int InsertSort(vector<int> nums) {
    if (nums.size() == 0 || nums.size() == 1)
        return 0;
    
    for(int i = 1; i < nums.size(); i++) {
        for(int j = i - 1; j>=0 && nums[j] > nums[j+1]; j--) {
            swap(nums[j],nums[j+1]);
        }
    }
    return 0;
}

