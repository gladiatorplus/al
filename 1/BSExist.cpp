#include <iostream>
#include <vector>
using namespace std;

bool BSExits(vector<int> nums,int aim) {
    if(nums.size() == 0) {
        return false;
    }

    
    int L = 0, R = nums.size()-1;

    while(L < R) {
        int mid = L + (R - L) >> 2; //(R - L) >> 2
        if(nums[mid] == aim) {
            return true;
        }

        if(nums[mid] < aim)  {
            L = mid+1;         //L = mid
            continue;
        }
        if(nums[mid] > aim) {
            R = mid-1;
            continue;
        }
    }

    return nums[L] == aim;
}
