#include <iostream>
#include "Graph_Node_Edge.h"
#include <list>
#include <queue>
#include <map>
#include <utility>

using namespace std;


list<Node*> TopologySort(Graph* gra) {
    list<Node*> res;

    if(gra == nullptr) {
        return res;
    }

    queue<Node*> zeroIn;
    map<Node*, int> mmap;   //xxj 记录Node 入度

    for(auto node : gra->nodes) {
        mmap.insert(pair<Node*,int>(node.second, node.second->in));         //xxj
        if(node.second->in == 0) {
            zeroIn.push(node.second);
        }
    }

    while(!zeroIn.empty()) {
        auto node = zeroIn.front();
        zeroIn.pop();
        res.push_back(node);

        for(Node* nnode : node->nexts) {       //xxj
            mmap.find(nnode)->second--;
            if(mmap.find(nnode)->second == 0) {
                zeroIn.push(nnode);
            }
        }
    }

    return res;
}
