#include <iostream>
#include <vector>
#include <map>
#include <set>

using namespace std;

typedef struct Node {
    int value;
    int in;
    int out;
    vector<Node*> nexts;
    vector<Edge*> edges;

    Node(int v) {
        this->value = v;
        this->in = this->out = 0;
        this->nexts = {};
        this->edges = {};
    }
}Node;


typedef struct Edge {
    int weight;
    Node* from;
    Node* to;
    Edge(int w, Node* from, Node* to) {
        this->weight = w;
        this->from = from;
        this->to = to;
    }
    
}Edge;

typedef struct Graph {
    map< int,Node*> nodes;  //注意自定义排序规则
    set<Edge*> edges;

    Graph() {
        this->nodes = {};
        this->edges = {};
    }
}Graph;