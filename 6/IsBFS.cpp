#include <iostream>
#include <Graph_Node_Edge.h>
#include <queue>
#include <set>

using namespace std;

void BFS(Node* head) {
    if(head == nullptr ) {
        return ;
    }

    queue<Node*> que;
    set<Node*> sset;

    que.push(head);
    sset.insert(head);

    while(que.size() > 0) {
        auto node = que.front();
        que.pop();
        printf("Node value: %d",node->value);

        for(auto nnode: node->nexts) {
            if(sset.find(nnode) != sset.end()) { //不重复
                continue;
            }
            sset.insert(nnode); 
            que.push(nnode);
        }

    }
}