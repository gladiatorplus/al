#include <iostream>
#include <list>
#include <stack>
using namespace std;

typedef struct Node {
    Node* next;
    int value;
    Node(int v) {
        this->value = v;
    }
}Node;


bool HuiWen(Node* head) {
    Node* oneStep = head->next;
    Node* twoStep = head;
    while(twoStep->next != nullptr && twoStep->next->next!= nullptr) {
        oneStep = oneStep->next;
        twoStep = twoStep->next->next;
    }

    stack<int> temp;
    while(oneStep != nullptr) {
        temp.push(oneStep->value);
        oneStep = oneStep->next;
    }

    int i = temp.size();
    while(i--) {
        if(head->value != temp.top()){
            return false;
        }
        temp.pop();
        head = head->next;
    }


}
