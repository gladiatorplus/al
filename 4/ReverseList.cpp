#include <iostream>
#include <list>
using namespace std;

typedef struct Node {
    int value;
    Node* next;
    Node(int v) { //构造
        this->value = v;
    }
}Node;

Node* Revert(Node* head) {
    if(head == nullptr || head->next == nullptr) {
        return nullptr;
    }
    Node* next;
    Node* pre;
    while(head != nullptr ) {  //pre -> head -> next
        next = head->next;
        head->next = pre;
        pre = head;
        head = next;
    }
    return pre;//xxj
}

typedef struct  DNode
{
    int value;
    DNode* Last;
    DNode* Next;
    DNode(int v) {
        this->value = v;
    }
};

DNode* Revert2(DNode* head) {
    if(head == nullptr || (head->Next == nullptr && head->Last == nullptr)) {
        return nullptr;
    }

    DNode* next;
    DNode* pre;
    while(head!=nullptr) {
        next = head->Next;
        head->Next = pre;
        head->Last = next;
        pre = head;
        head = next;
    }

    return pre;
}


