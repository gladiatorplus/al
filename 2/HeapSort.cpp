#include <iostream>
#include <vector>
using namespace std;


void HeapSort(vector<int> nums) {
    if(nums.size() == 0 || nums.size() ==1 ) {
        return ;
    }

    for(int i = 0; i < nums.size();i++) {
        HeapInsert(nums,i);
    }

    int size = nums.size();
    swap(nums[0],nums[--size]);
    while(size > 0) {
        Heapify(nums,0,size);//size
        swap(nums[0],nums[--size]);
    }

}

void HeapInsert(vector<int> nums , int index) {
    while(nums[(index-1)/2 < nums[index]]) {
        swap(nums[(index-1)/2],nums[index]);
    }
}

void Heapify(vector<int> nums,int index,int size) {  //int size
    while((index*2 + 1) < size) { //index*2 +2 
        int largest = (index*2+2) < size && nums[index*2+1] < nums[index*2+2] ? index*2+2:index*2+1;//(index*2+2) < nums.size() && 
        if(nums[largest] > nums[index]) {
            swap(nums[largest],nums[index]);
            index = largest;
            continue;
        }else{
            break;
        }
    }
}