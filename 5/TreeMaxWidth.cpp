#include <iostream>
#include <queue>
#include <map>
using namespace std;

typedef struct Node {
    int value;
    Node* left;
    Node* right;
    Node(int v) {
        this->value = v;
    }
}Node;

int TreeMaxWidth(Node* head){
    if(head == nullptr) {
        return 0;
    }
    map<Node*,int> levelMap;
    levelMap.insert(pair<Node*,int>(head,1)); //1 level
    queue<Node*> levelQue;
    levelQue.push(head);
    int curlLevel = 0;
    int maxWidth = 0;
    int curWidth = 0;
    while(!levelQue.empty()) { //head != nullptr
        Node* node = levelQue.front();
        levelQue.pop();
        if(node->left != nullptr) {
            levelMap.insert(pair<Node*,int>(node->left,levelMap[node] + 1));
            levelQue.push(node->left);
        }
        if(node->right != nullptr) {
            levelMap.insert(pair<Node*,int>(node->right,levelMap[node] + 1));
            levelQue.push(node->right);
        }

        if(levelMap[node] > curlLevel) {
            curlLevel = levelMap[node];
            maxWidth = curWidth > maxWidth ? curWidth:maxWidth;
            curWidth = 0;                                                       //xxj
        }else {
            curWidth ++;
        }
    }

    maxWidth = curWidth > maxWidth ? curWidth:maxWidth;

    return maxWidth;
}