#include <iostream>
#include <queue>
using namespace std;

typedef struct Node {
    Node* left;
    Node* right;
    int value;
    Node(int v ) {
        this->value = v;
    }
}Node;

bool IsCBT(Node* head) {
    if(head == nullptr) {
        return true;
    }

    queue<Node*> que;
    que.push(head);
    Node* left = nullptr;
    Node* right = nullptr;
    bool leaf =  false;

    while(que.size()!= 0) {
        Node* node = que.front();
        que.pop();
        left = node->left;
        right = node->right;

        if((leaf && (left != nullptr || right != nullptr) || (left == nullptr && right != nullptr))) { //xxj
            return false;
        }

        if(left!= nullptr) {
            que.push(left);
        }
        if(right != nullptr) {
            que.push(right);
        }else {
            leaf = true;
        }
    }
}