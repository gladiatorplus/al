#include <iostream>
#include <list>
using namespace std;

typedef struct Node {
    int value;
    Node* left;
    Node* right;
    Node(int v) {
        this->value = v;
    }
}Node;

void PreTraverse(Node* head) {
    if(head == nullptr) {
        return;
    }

    printf("%d/n",head->value);
    PreTraverse(head->left);
    PreTraverse(head->right);
}

void InTraverse(Node* head) {
    if(head == nullptr) {
        return;
    }

    InTraverse(head->left);
    printf("%d/n",head->value);
    InTraverse(head->right);
}

void PosTraverse(Node* head) {
    if(head == nullptr) {
        return;
    }

    PosTraverse(head->left);
    PosTraverse(head->right);
    printf("%d/n",head->value);
}