#include <iostream>
#include <math.h>

using namespace std;

typedef struct Node {
    Node* left;
    Node* right;
    int value;
    Node(int v) {
        this->value = v;
    }
}Node;

bool IsBalanceTree(Node* head){
    if(head == nullptr) {
        return true;
    }

}

pair<bool, int> process(Node* head) {
    if(head == nullptr) {
        return pair<bool,int>(true,0);
    }

    auto leftData = process(head->left);
    auto rightData = process(head->right);

    return pair<bool,int>(leftData.first && rightData.first && 
        abs(leftData.second-rightData.second) < 2, max(leftData.second,rightData.second));
}