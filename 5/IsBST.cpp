//是否是二叉查询树,1、左节点及以下节点的值比它小；2、右节点及以下节点的值比它大
#include <iostream>
#include <vector>
using namespace std;

typedef struct Node {
    Node* left;
    Node* right;
    int value;
    Node(int v ) {
        this->value = v;
    }
}Node;

bool IsBST(Node* head) {
    if(head == nullptr) {
        return true;
    }

    vector<Node*> oderVec;
    process(head,oderVec);
    int curValue = INT64_MIN;//0
    for(auto node:oderVec) {
        if(node->value < curValue){
            return false;
        }

        curValue = node->value;
    }

}
void process(Node* head, vector<Node*>& orderVec) {
    if(head == nullptr) {
        return;
    }

    process(head->left,orderVec);
    orderVec.push_back(head);
    process(head->right,orderVec);
}